package com.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.login.model.User;
import com.login.security.JwtUtil;

@RestController
public class LoginController {

//	@Autowired
//	JwtUtil jwtUtil;
	
	JwtUtil jwtUtil =new JwtUtil();
	
	@RequestMapping("/getToken/{userId}/{username}/{type}")
	public String getToken(@PathVariable("userId") int userId,@PathVariable("username") String uname,@PathVariable("type") String type) {
		return jwtUtil.generateToken(uname,type,userId);
	}
	
	@RequestMapping("/valiadte/{token}")
	public boolean validateToken(@PathVariable("token") String token) {
		return jwtUtil.isTokenValid(token);
	}
}
