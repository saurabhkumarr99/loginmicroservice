package com.login.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.login.model.User;
import com.login.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	@GetMapping("/getUserById/{userId}")
	public Optional<User> getByUserType(@PathVariable("userId") int userId) {
		return userService.getUserbyId(userId);
	}

	@PostMapping("/registerUser")
	public String registerUser(@RequestBody User user) {

		User regUser = userService.registerUser(user);

		if (regUser != null)
			return regUser + "\nUser Registered into the Database";

		return "User Registeration Failed!!!";
	}

	@DeleteMapping("/deleteUser/{userId}")
	public String deleteUser(@PathVariable("userId") int userId) {
		return userService.deleUser(userId);
	}
}
