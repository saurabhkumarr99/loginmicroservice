package com.login.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.login.dao.UserRepository;
import com.login.model.User;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	// Get all User
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	// Get User by id
	public Optional<User> getUserbyId(int id) {
		return userRepository.findById(id);
	}

	// Register User
	public User registerUser(User user) {
		return userRepository.save(user);
	}

	// Delete User
	public String deleUser(int id) {
		userRepository.deleteById(id);
		return "User Deleted";
	}
}
