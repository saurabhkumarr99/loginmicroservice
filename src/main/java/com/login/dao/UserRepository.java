package com.login.dao;

import org.springframework.data.cassandra.repository.CassandraRepository;

import com.login.model.User;

public interface UserRepository extends CassandraRepository<User, Integer>{

}
